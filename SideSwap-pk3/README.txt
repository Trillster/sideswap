Implements Valorant and CS's halftime and win by 2 mechanics to allow teams to
play from both set of team spawns equally. Supports both overtime and sudden death
rulesets with the default being 2 overtime sets into sudden death.

Use `trill_ss_sv_overtimeCap` to determine the number of overtime rounds that should
be played. Use a value of -1 for infinite overtime.

Created by Trillster to test new features of Zandronum 3.2
Props again to Kaminsky for implementing features required to make this work properly